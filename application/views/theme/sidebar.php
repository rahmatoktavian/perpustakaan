<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-book"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Pustaka <sup>App</sup></div>
  </a>

  <?php if($this->session->userdata('type') == 'anggota'):?>
  <li class="nav-item">
    <a class="nav-link" href="<?php echo site_url('peminjaman_saya/read');?>">
      <i class="fas fa-fw fa-book"></i>
      <span>Peminjaman Saya</span>
    </a>
  </li>
  <?php endif;?>

  <?php if($this->session->userdata('type') == 'admin'):?>
  <li class="nav-item active">
    <a class="nav-link" href="<?php echo site_url('dashboard/index');?>">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="<?php echo site_url('api_client/rajaongkir');?>">
      <i class="fas fa-fw fa-truck"></i>
      <span>API RajaOngkir</span></a>
  </li>
  <?php endif;?>

  <?php if($this->session->userdata('type') == 'petugas'):?>
  <li class="nav-item">
    <a class="nav-link" href="<?php echo site_url('peminjaman/read');?>">
      <i class="fas fa-fw fa-upload"></i>
      <span>Peminjaman</span>
    </a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="<?php echo site_url('pengembalian/read');?>">
      <i class="fas fa-fw fa-download"></i>
      <span>Pengembalian</span>
    </a>
  </li>
  <?php endif;?>

  <!-- Laporan -->
  <?php if($this->session->userdata('type') == 'petugas'):?>
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu_grafik" aria-expanded="true" aria-controls="menu_grafik">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Grafik</span>
    </a>
    <div id="menu_grafik" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="<?php echo site_url('grafik/rekap_peminjaman');?>">
          <i class="fas fa-fw fa-calendar"></i>
          Rekap Peminjaman
        </a>
      </div>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu_laporan" aria-expanded="true" aria-controls="menu_laporan">
      <i class="fas fa-fw fa-file"></i>
      <span>Laporan</span>
    </a>
    <div id="menu_laporan" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="<?php echo site_url('laporan/rekap_peminjaman');?>">
          <i class="fas fa-fw fa-clone"></i>
          Rekap Peminjaman
        </a>
        <a class="collapse-item" href="<?php echo site_url('laporan/detail_peminjaman');?>"> 
          <i class="fas fa-fw fa-list"></i>
          Detail Peminjaman
        </a>
      </div>
    </div>
  </li>
  <?php endif;?>

  <!-- Input -->
  <?php if($this->session->userdata('type') == 'admin' || $this->session->userdata('type') == 'petugas'):?>
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu_setting" aria-expanded="true" aria-controls="menu_setting">
      <i class="fas fa-fw fa-cog"></i>
      <span>Setting</span>
    </a>
    <div id="menu_setting" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">

        <?php if($this->session->userdata('type') == 'admin'):?>
        <a class="collapse-item" href="<?php echo site_url('user/read');?>">
          <i class="fas fa-fw fa-users"></i>
          User
        </a>
        <a class="collapse-item" href="<?php echo site_url('petugas/read');?>">
          <i class="fas fa-fw fa-user-circle"></i>
          Petugas
        </a>
        <?php endif;?>

        <?php if($this->session->userdata('type') == 'petugas'):?>
        <a class="collapse-item" href="<?php echo site_url('anggota/read');?>">
          <i class="fas fa-fw fa-user"></i>
          Anggota
        </a>
        
        <a class="collapse-item" href="<?php echo site_url('buku/read');?>">
          <i class="fas fa-fw fa-book"></i>
          Buku
        </a>
        <a class="collapse-item" href="<?php echo site_url('kategori_buku/read');?>">
          <i class="fas fa-fw fa-folder"></i>
          Kategori Buku
        </a>
        <?php endif;?>

      </div>
    </div>
  </li>
  <?php endif;?>

  <hr class="sidebar-divider d-none d-md-block">

  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>